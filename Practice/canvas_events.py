import tkinter as tk
import random

class Shape():
    def __init__(self, id):
        self.x = 0
        self.y = 0
        self.color = "black"
        self.id = id

class Ball(Shape):
    def __init__(self,id, x, y, r):
        super().__init__(id)
        self.r = r
        self.x = x
        self.y = y 
        self.xspeed = 0
        self.yspeed = 0
        
class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Шары")
        self.geometry("800x500")
        self.w = 800
        self.h = 600
        self.bind('<Configure>', self.resize)
        self.bind('<KeyPress>', self.on_key_press)
        self.bind('<KeyRelease>', self.on_key_release)
        self.balls = []
        self.keyPressed = {'Up': False, 'Down': False,'Left': False,'Right': False}
        btn_stop = tk.Button(self, text = "Stop!")
        btn_stop.pack()
        btn_stop["command"] = self.btn_stop_move
        btn_restart = tk.Button(self, text = "Restart!")
        btn_restart.pack()
        btn_restart["command"] = self.btn_restart_move
        self.__init_widgets()
        

    def __init_widgets(self):
        self.canvas = tk.Canvas(self, bg = "grey")
        self.canvas.pack(fill = tk.BOTH, expand = True)
        self.canvas.bind("<Button-1>", self.canvas_on_click)
        ball_id =  self.canvas.create_oval((100, 100, 100 + 50 , 100 + 50), fill = "red")
        self.player = Ball(ball_id, 125, 125, 25)
        self.balls.append(self.player)
        for i in range(2):
            x = random.randint(25,400)
            y = random.randint(25,400)
            ball_id =  self.canvas.create_oval((x, y, x + 50 , y + 50), fill = "white")
            ball = Ball(ball_id, x+25, y+25, 25)
            ball.xspeed = random.randint(-2,2)
            ball.yspeed = random.randint(-2,2)
            self.balls.append(ball)
            pass
        self.animate()

    def animate(self):
        self.__input()
        ballNumber = 0
        for ball in self.balls:
            ballNumber += 1
            for i in range(ballNumber, len(self.balls)):
                otherBall = self.balls[i]
                balls_dest = (otherBall.x - ball.x)**2 +\
                    (otherBall.y - ball.y)**2
                if balls_dest <= (otherBall.r + ball.r)**2:
                    ball.xspeed, ball.yspeed, otherBall.xspeed, otherBall.yspeed = otherBall.xspeed, otherBall.yspeed, ball.xspeed, ball.yspeed

        for ball in self.balls:
            self.canvas.move(ball.id, ball.xspeed, ball.yspeed)
            ball.x += ball.xspeed
            ball.y += ball.yspeed
            
            
            if ball.x - ball.r < 0 or \
                ball.x + ball.r > self.w:
                ball.xspeed = - ball.xspeed
            if ball.y - ball.r < 0 or \
                ball.y + ball.r > self.h:
                ball.yspeed = -ball.yspeed
        
        self.canvas.after(33, self.animate)

    def btn_stop_move(self):
        for ball in self.balls:
            ball.xspeed = 0
            ball.yspeed = 0

    def btn_restart_move(self):
        for ball in self.balls:
            ball.xspeed = random.randint(-10,10)
            ball.yspeed = random.randint(-10,10)

    def resize(self, event):
        self.w = event.width
        self.h = event.height

    def canvas_on_click(self, event):
        ballIds = self.canvas.find_withtag(tk.CURRENT)
        for ballId in ballIds:
            for ball in self.balls:
                if ball.id == ballId:
                    self.canvas.delete(ball.id)
                    self.balls.remove(ball)

    def on_key_press(self, event):
        self.keyPressed[event.keysym] = True
    
    def on_key_release(self, event):
        self.keyPressed[event.keysym] = False
    
    def __input(self):
        if self.keyPressed['Up']:
            self.player.yspeed = - 3
        elif self.keyPressed['Down']:
            self.player.yspeed = 3
        else:
            self.player.yspeed = 0
        if self.keyPressed['Left']:
            self.player.xspeed = - 3
        elif self.keyPressed['Right']:
            self.player.xspeed = 3
        else:
            self.player.xspeed = 0
        

    
if __name__ == "__main__":
    app = App()
    app.mainloop()



'''Для перевода в .exe (в cmd):
cd "название папки" 
набираем:
build_exe canvas_events.py  -W setup.py - c --bundle-files 3
жмём enter
Появляется setup.py, его открываем
...


build.exe setup.py



