import sqlite3

def db_connect() -> sqlite3.connect:
    con = sqlite3.connect("tbot.db")
    return con

def db_disconnect(con: sqlite3.connect):
    con.close()

def db_init(con: sqlite3.connect):
    """Инициализация БД:
    - создание таблиц
            )""")
    con.commit()

def get_currencies_by_chat(chat_id, con: sqlite3.connect):
def get_currencies_by_chat(chat_id, con: sqlite3.connect) -> list:
    cur = con.cursor()
    cur.execute("select currency from Currencies where chat_id=?", chat_id)
    cur.execute("select currency from Currencies where chat_id=?", (chat_id,))
    currencies_table = cur.fetchall()
    currencies = []
    for currency in currencies_table:
        currencies.append(currency[0])
    
    return currencies
    return currencies

def add_currency_by_chat(chat_id, currency, con: sqlite3.connect):
    cur = con.cursor()
    cur.execute("""INSERT INTO Currencies('chat_id', 'currency')
    VALUES(?,?)
    """, (chat_id, currency))
    con.commit()

def delete_currency_by_chat(chat_id, currency, con: sqlite3.connect):
    cur = con.cursor()
    cur.execute("""DELETE FROM Currencies 
    WHERE chat_id=? AND currency=?
    """, (chat_id, currency))
    con.commit()

def get_all_chats(con: sqlite3.connect) -> list:
    cur = con.cursor()
    cur.execute("SELECT DISTINCT chat_id FROM Currencies")
    chats_table = cur.fetchall()
    chats = []
    for chat in chats_table:
        chats.append(chat[0])
    
    return chats


    