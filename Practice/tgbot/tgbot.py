import telebot
import currencies
import db
import schedule
import threading
import time

#t.me/BotofcurrenciesAG_bot
#https://core.telegram.org/bots/api

db_con = db.db_connect()
db.db_init(db_con)
db.db_disconnect(db_con)

bot = telebot.TeleBot("1712221177:AAGnKxUXosmYCKvQBuM7NI3bdcXev7X5KYg")

currencies_list = ['USD']
def get_rates_by_chat(chat_id):
    con = db.db_connect()
    currencies_list = db.get_currencies_by_chat(chat_id, con)
    db.db_disconnect(con)
    data = currencies.get_exchanges(currencies_list)
    msg = "\n".join(data)
    if (len(msg) == 0):
        msg = "Не указаны валюты"
    bot.send_message(chat_id, msg)

@bot.message_handler(commands=['start'])
def send_welcome(message):

@bot.message_handler(commands=['getrates'])
def get_rates(message):
    data = currencies.get_exchanges(currencies_list)
    msg = "\n".join(data)
    bot.send_message(message.chat.id, msg)
    get_rates_by_chat(message.chat.id)

@bot.message_handler(commands=['add'])
def add_currency(message):
    msg = message.text[4:].strip()
    currencies_list.append(msg)
    con = db.db_connect()
    db.add_currency_by_chat(message.chat.id, msg, con)
    db.db_disconnect(con)
    bot.send_message(message.chat.id, f"Добавлена валюта {msg}")

@bot.message_handler(commands=['del'])
def del_currency(message):
    msg = message.text[4:].strip()
    con = db.db_connect()
    currencies_list = db.get_currencies_by_chat(message.chat.id, con)
    if (msg in currencies_list):
        currencies_list.remove(msg)
        db.delete_currency_by_chat(message.chat.id, msg, con)
        bot.send_message(message.chat.id, f"Удалена валюта {msg}")
    else:
        bot.send_message(message.chat.id, f"Нет валюты {msg}")
    db.db_disconnect(con)

@bot.message_handler(func=lambda m: True)
def echo_all(message):
	bot.reply_to(message, message.text)


def send_currencies():
    con = db.db_connect()
    chats = db.get_all_chats(con)
    for chat in chats:
        get_rates_by_chat(chat)
    db.db_disconnect(con)

def schedule_worker():
    while True:
        schedule.run_pending()
        time.sleep(10)

schedule.every().day.at("13:00").do(send_currencies)
schedule_thread = threading.Thread(target=schedule_worker)
schedule_thread.start()

bot.polling()




