import tkinter as tk
import socket as s
import threading
import os

class ChatWindow(tk.Frame):
    def __init__(self, root, sock):
        super().__init__(root)
        self.root = root
        self.socket = sock
        self.grid(row = 0, column = 0, sticky ="nsew")
        self.messages = tk.Text(self)
        self.messages.grid(row = 0, column = 0, columnspan = 4, sticky ="nsew")
        self.chat_message = tk.Text(self, height = 1)
        self.chat_message.grid(row = 1, column = 0, sticky ="nsew")
        self.btn = tk.Button(self, text ="\u27A1", font = "Arial 24")
        self.btn.grid(row =1, column = 2, columnspan =3, sticky ="nsew")
        self.btn["command"]=self.sendMessage
        self.vscrollbar = tk.Scrollbar(self, orient ="vert", command =self.messages.yview)
        self.messages["yscrollcommand"] = self.vscrollbar.set
        self.vscrollbar.grid(row = 0, column = 4, sticky = "ns")

        self.vscrollbar1 = tk.Scrollbar(self, orient ="vert", command =self.chat_message.yview)
        self.chat_message["yscrollcommand"] = self.vscrollbar1.set
        self.vscrollbar1.grid(row = 1, column = 1, sticky = "ns")

        self.rowconfigure(0, weight =1)
        self.columnconfigure(0, weight =1)
        self.chat_message.bind("<Return>",self.sendMessage2)

    def sendMessage2(self, event):
        self.sendMessage()

    def sendMessage(self):
        msg = self.chat_message.get("1.0", tk.END)
        self.chat_message.delete("1.0", tk.END)
        soc.sendall(msg.encode("utf-8"))

    def receiveMessage(self, msg):
        self.messages.insert(tk.END, msg)
        self.messages.see(tk.END)




HOST = "localhost"
PORT = 12345
def client_receive(soc: s.socket):
    
    life = True
    while life:
        try:
            data = soc.recv(4096)
            print("\r<",end ="")
            a = 3
            data1 = data.decode("utf-8")
            c = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZzАаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЭэЮюЯя:;!?., '
            res = []
            len_c=len(c)
            for i in data1:
                res.append(c[(c.find(i)-a)%len_c]) 
            print(''.join(res), sep = '')
            print(">", end="")
            print(data1)
            chat.receiveMessage(data1)
        except:
            life = False
            print("close")


soc = s.socket()

try:
    soc.connect((HOST, PORT))
except Exception as ex:
    print("Ошибка подключения: ", ex)
    exit(1)

receive_thread = threading.Thread(target=client_receive, args=(soc,))
receive_thread.start()
UsrNm = input("Введите ваше имя пользователя: ")
"""while True:
    msg = input(">")
    UsrNme = str(UsrNm) + ": "
    a = 3
    c = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZzАаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЭэЮюЯя:;!?., '
    res = []
    len_c=len(c)
    for i in UsrNme:
        res.append(c[(c.find(i)+a)%len_c]) 
    UserName = ''.join(res)
    a = 3
    c = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZzАаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЭэЮюЯя:;!?., '
    res = []
    len_c=len(c)
    for i in msg:
        res.append(c[(c.find(i)+a)%len_c]) 
    message = ''.join(res)
    if message == "\\quit":
        break
    soc.sendall(str(UserName).encode("utf-8") + message.encode("utf-8"))
"""
root = tk.Tk()
chat = ChatWindow(root,soc)
root.rowconfigure(0, weight =1)
root.columnconfigure(0, weight =1)
chat.mainloop()

soc.close()
receive_thread.join()
