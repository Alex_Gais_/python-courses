import socket as s
import threading as t

HOST = "0.0.0.0"
PORT = 12345

def sendMassengeAllOthers(msg, client_soc):
    for client in clients:
        if client != client_soc:
            client.sendall(msg)


def client_thread(client_soc:s.socket):
    life = True
    while life:
        try:
            msg = client_soc.recv(4096)
            print(msg.decode("utf-8"))
            sendMassengeAllOthers(msg, client_soc)
        except:
            life = False
    clients.remove(client_soc)

soc = s.socket()
soc.bind((HOST, PORT))
soc.listen(1)
clients = []

while True:
    client_soc, client_addr = soc.accept()
    clients.append(client_soc)
    print('Новое подключение с адреса {}:{}'.format(*client_addr))
    client_t = t.Thread(target=client_thread, args=(client_soc,))
    client_t.start()

