class Matrix:
    '''Класс для работы с матрицами'''
    def __init__(self,matrix:list):
        self.matrix = matrix
    def getter(self):
        return matrix
    def __add__(self,other):
        result = []
        for i in range(len(self.matrix)):
            row = []
            for j in range(len(self.matrix[i])):
                if len(self.matrix) == len(other.matrix)\
                    and len(self.matrix[i]) == len(other.matrix[i]):
                    s = self.matrix[i][j] + other.matrix[i][j]
                    row.append(s)
                else:
                    raise Exception("Ошибка, матрицы должны быть одинакового размера!")
            tup = tuple(row)
            result.append(tup)
        return Matrix(result)
    def __sub__(self,other):
        result = []
        for i in range(len(self.matrix)):
            row = []
            for j in range(len(self.matrix[i])):
                if len(self.matrix) == len(other.matrix)\
                    and len(self.matrix[i]) == len(other.matrix[i]):
                    s = self.matrix[i][j] + other.matrix[i][j]
                    row.append(s)
                else:
                    raise Exception("Ошибка, матрицы должны быть одинакового размера!")
            tup = tuple(row)
            result.append(tup)
        return Matrix(result)
    def __mul__(self,other):
        result = []
        for i in range(len(self.matrix)):
            row = []
            for j in range(len(self.matrix[i])):
                s = self.matrix[i][j] * other.matrix[i][j]
                row.append(s)
            tup = tuple(row)
            result.append(tup)
        return Matrix(result)
    def __str__(self):
        s = ""
        for row in self.matrix:
            for el in row:
                s += str(el) + "\t"
            s += "\n"
        return s
    def save(self,fileName:str):
        with open(fileName,"w") as f:
            f.write(self.__str__())
    @staticmethod
    def load(fileName:str):
        '''Возвращает объект Matrix'''
        matrix = []
        try:
            with open(fileName) as f:
                f.readline
                for line in f:
                    row = []
                    if line.endswith("\n"):
                        line = line[0:-1]
                    rowStr = line.split("\t")
                    for el in rowStr:
                        row.append(int(el))
                    tup = tuple(row)
                    matrix.append(tup)
        except IOError:
            print("Ошибка доступа к файлу.Проверьте правильность пути и наличие файла")
        except Exception as ex:
            print("Всё пропало!")
            print(ex)
        else:
            print("Матрица успешно загружена!")
        return Matrix(matrix)
    