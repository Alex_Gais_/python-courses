import tkinter as tk
import tkinter.messagebox as mb
import tkinter.filedialog as fd

class SomeWindow(tk.Toplevel):
    def __init__(self,master):
        super().__init__(master)
        self.title("Окошко")
        self.geometry("640x480")
        self.__init_widgets()
    
    def __init_widgets(self):
        label = tk.Label(self, text = "Ну, просил!", fg = "red")
        label.pack()


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Окошки")
        self.geometry("640x480")
        self.__init_widgets()

    def __init_widgets(self):
        btn = tk.Button(self, text = "Не нажимать!")
        btn["command"] = self.btn_on_click
        btn.pack()
        btnOpen = tk.Button(self, text = "Выбрать файл")
        btnOpen["command"] = self.btnOpen_on_click
        btnOpen.pack()
    
    def btnOpen_on_click(self):
        f = fd.askopenfile(self)
        print(f.name)
        f.readall()
        pass
    def btn_on_click(self):
        if mb.askyesno("ВНИМАНИЕ!!!", "Сказано: не нажимать!\nПродолжить?"):
            win = SomeWindow(self)
            win.grap_set()

if __name__ == "__main__":
    app = App()
    app.mainloop()