import tkinter as tk
import random

class Shape():
    def __init__(self, id):
        self.x = 0
        self.y = 0
        self.color = "black"
        self.id = id

class Ball(Shape):
    def __init__(self,id, x, y, r):
        super().__init__(id)
        self.r = r
        self.x = x + r
        self.y = y + r
        self.xspeed = 0
        self.yspeed = 0
        
class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Шары")
        self.geometry("800x500")
        self.w = 800
        self.h = 600
        self.bind('<Configure>', self.resize)
        self.balls = []
        btn_stop = tk.Button(self, text = "Stop!")
        btn_stop.pack()
        btn_stop["command"] = self.btn_stop_move
        btn_restart = tk.Button(self, text = "Restart!")
        btn_restart.pack()
        btn_restart["command"] = self.btn_restart_move
        self.__init_widgets()
        

    def __init_widgets(self):
        self.canvas = tk.Canvas(self, bg = "grey")
        self.canvas.pack(fill = tk.BOTH, expand = True)
        for i in range(5):
            x = random.randint(25,400)
            y = random.randint(25,400)
            ball_id =  self.canvas.create_oval((x, y, x + 50 , y + 50), fill = "white")
            ball = Ball(ball_id, x, y, 25)
            ball.xspeed = random.randint(1,10)
            ball.yspeed = random.randint(1,10)
            self.balls.append(ball)
            pass
        self.animate()

    def animate(self):
        ballNumber = 0
        for ball in self.balls:
            ballNumber += 1
            for i in range(ballNumber, len(self.balls)):
                otherBall = self.balls[i]
                balls_dest = (otherBall.x - ball.x)**2 +\
                    (otherBall.y - ball.y)**2
                if balls_dest <= (otherBall.r + ball.r)**2:
                    ball.xspeed, ball.yspeed, otherBall.xspeed, otherBall.yspeed = otherBall.xspeed, otherBall.yspeed, ball.xspeed, ball.yspeed
            '''otherBallIds = self.canvas.find_overlapping(
                ball.x - ball.r
                ball.y - ball.r
                ball.x + ball.r
                ball.y + ball.r)'''

            '''for otherBallId in otherBallIds:
                for otherBall in self.balls:
                    if otherBall.id == otherBallId:
                        '''
            '''
            
                
                otherBall = self.balls[i]
                if ball != otherBall and \
                   
                   #ball.x + ball.r > otherBall.x + otherBall.r and \
                   #ball.y + ball.r > otherBall.y + otherBall.r and \
                   #ball.x - ball.r < otherBall.x + otherBall.r and \
                   #ball.y - ball.r < otherBall.y + otherBall.r or \
                   #ball.x + ball.r > otherBall.x - otherBall.r and \
                   #ball.y + ball.r > otherBall.y - otherBall.r and \
                   #ball.x - ball.r < otherBall.x - otherBall.r and \
                   #ball.y - ball.r < otherBall.y - otherBall.r or \
                   #ball.x + ball.r > otherBall.x + otherBall.r and \
                   #ball.y + ball.r > otherBall.y - otherBall.r and \
                   #ball.x - ball.r < otherBall.x + otherBall.r and \
                   #ball.y - ball.r < otherBall.y - otherBall.r or \
                   #ball.x + ball.r > otherBall.x - otherBall.r and \
                   #ball.y + ball.r > otherBall.y + otherBall.r and \
                   #ball.x - ball.r < otherBall.x - otherBall.r and \
                   #ball.y - ball.r < otherBall.y + otherBall.r:
                   '''

        for ball in self.balls:
            self.canvas.move(ball.id, ball.xspeed, ball.yspeed)
            ball.x += ball.xspeed
            ball.y += ball.yspeed
            
            
            if ball.x - ball.r < 0 or \
                ball.x + ball.r > self.w:
                ball.xspeed = - ball.xspeed
            if ball.y - ball.r < 0 or \
                ball.y + ball.r > self.h:
                ball.yspeed = -ball.yspeed
        
        self.canvas.after(33, self.animate)

    def btn_stop_move(self):
        for ball in self.balls:
            ball.xspeed = 0
            ball.yspeed = 0

    def btn_restart_move(self):
        for ball in self.balls:
            ball.xspeed = random.randint(1,10)
            ball.yspeed = random.randint(1,10)

    def resize(self, event):
        self.w = event.width
        self.h = event.height

if __name__ == "__main__":
    app = App()
    app.mainloop()