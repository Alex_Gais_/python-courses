import tkinter as tk
import random

class GameState():
    player1_y = 150
    player2_y = 150
    ball_x = 250
    ball_y = 150
    ball_r = 10
    ball_x_speed = 2
    ball_y_speed = 2
    player1_score = 0
    player2_score = 0

class Game(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Pong")
        self.resizable(False, False)
        self.width = 500
        self.height = 300
        self.geometry(f"{self.width}x{self.height}")
        self.canvas = tk.Canvas(self, bg="green4")
        self.canvas.pack(fill=tk.BOTH, expand=True)
        self.game_state = GameState()
        self.out_line = 25
        self.player_height = 80
        self.bind('<KeyPress>', self.on_key_press)
        self.bind('<KeyRelease>', self.on_key_release)
        self.keyPressed = {
            'Up':False,
            'Down':False,
            'w':False,
            's':False,
            }
        self.__restart()
        self.__game_run()


    def on_key_press(self, event):
        self.keyPressed[event.keysym] = True

    def on_key_release(self, event):
        self.keyPressed[event.keysym] = False

    def __input(self):
        if self.keyPressed['Up']:
            self.game_state.player2_y-=10
        elif self.keyPressed['Down']:
            self.game_state.player2_y+=10     

        if self.keyPressed['w']:
            self.game_state.player1_y-=10
        elif self.keyPressed['s']:
            self.game_state.player1_y+=10

    def __restart(self):
        gs = self.game_state
        gs.player1_y = self.height/2
        gs.player2_y = self.height/2
        gs.ball_x = self.width/2
        gs.ball_y = self.height/2
        gs.ball_y_speed = random.randint(-3,3)
        gs.ball_x_speed = random.randint(-3,3)
        if gs.ball_x_speed == 0: 
            gs.ball_x_speed = 2

    def __update(self):
        gs = self.game_state
        gs.ball_y += gs.ball_y_speed
        gs.ball_x += gs.ball_x_speed

        if gs.ball_y - gs.ball_r < 0 or \
            gs.ball_y + gs.ball_r > self.height: 
            gs.ball_y_speed = -gs.ball_y_speed   

        if gs.ball_x - gs.ball_r < self.out_line or \
            gs.ball_x + gs.ball_r > self.width-self.out_line: 
            
            if (gs.ball_x > self.width/2):
                #player 2
                if  gs.ball_y <= gs.player2_y + self.player_height/2 and \
                    gs.ball_y >= gs.player2_y - self.player_height/2:
                    gs.ball_x_speed = -gs.ball_x_speed 
                    speed_increase = random.randint(1,3)
                    gs.ball_x_speed -= speed_increase
                    if gs.ball_y_speed > 0: gs.ball_y_speed += speed_increase
                    else: gs.ball_y_speed -= speed_increase
                else:
                    gs.player1_score += 1
                    self.__restart()
            else:
                #player 1
                if  gs.ball_y <= gs.player1_y + self.player_height/2 and \
                    gs.ball_y >= gs.player1_y - self.player_height/2:
                    gs.ball_x_speed = -gs.ball_x_speed 
                    speed_increase = random.randint(1,3)
                    gs.ball_x_speed += speed_increase
                    if gs.ball_y_speed > 0: gs.ball_y_speed += speed_increase
                    else: gs.ball_y_speed -= speed_increase
                else:
                    gs.player2_score += 1
                    self.__restart()

    def __draw(self):
        self.canvas.delete("all")
        gs = self.game_state
        self.canvas_ball = self.canvas.create_oval(
            gs.ball_x-gs.ball_r,
            gs.ball_y-gs.ball_r,
            gs.ball_x+gs.ball_r,
            gs.ball_y+gs.ball_r,
            fill="yellow"
        )

        self.canvas_p1 = self.canvas.create_rectangle(
            10,
            gs.player1_y - self.player_height/2,
            self.out_line,
            gs.player1_y + self.player_height/2,
            fill="red"
        )

        self.canvas_p2 = self.canvas.create_rectangle(
            self.width-self.out_line,
            gs.player2_y - self.player_height/2,
            self.width-10,
            gs.player2_y + self.player_height/2,
            fill="blue"
        )
        
        self.canvas.create_text(self.width/2, 20,
                text=f"{self.game_state.player1_score}:{self.game_state.player2_score}",
                justify=tk.CENTER, font="Arial 16")

    def __game_run(self):
        self.__input()
        self.__update()
        self.__draw()

        self.canvas.after(33, self.__game_run)

game = Game()
game.mainloop()
