import tkinter as tk
import tkinter.messagebox as mb
import tkinter.filedialog as fd
import socket as s
import threading as t

#functional of main programm--------------------------------------------------------------------------------------
class Ship():
    def __init__(self, id, x01, x02, y01, y02, r):
        super().__init__()
        self.x01 = x01
        self.x02 = x02
        self.y01 = y01
        self.y02 = y02
        self.r = r
        self.id = id
        
class ClientGameWindow(tk.Frame):
    def __init__(self, master, soc):
        super().__init__(master)
        self.socket = soc
        self.title("Морской бой")
        self.geometry("900x600")
        self.w = 900
        self.h = 600
        self.bind('<Configure>', self.resize)
        self.ships = []
        self.canvas = tk.Canvas(self, bg = "blue")
        self.btn_start_game = tk.Button(self, text = "Старт!")
        self.btn_start_game.pack()
        self.canvas.pack(fill = tk.BOTH, expand = True)
        y = 35
        x = 65
        letters = "АБВГДЕЖЗИК"
        for i in range(10):
            self.canvas.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
            i += 1 
            x += 30

        y = 65
        x = 35
        numbers = 1               
        for j in range(10):
            self.canvas.create_text(x, y, text=str(numbers), font="TimesNewRoman")
            numbers += 1
            y += 30

        y1 = 50
        y2 = 80                
        for j in range(10):
            x1 = 50
            x2 = 80
            for i in range(10):
                self.canvas.create_rectangle(x1, y1, x2, y2, outline = 'white')
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30

        '''y = 35
        x = 435
        letters = "АБВГДЕЖЗИК"
        for i in range(10):
            self.canvas.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
            i += 1 
            x += 30

        y = 65
        x = 405
        numbers = 1               
        for j in range(10):
            self.canvas.create_text(x, y, text=str(numbers), font="TimesNewRoman")
            numbers += 1
            y += 30

        y1 = 50
        y2 = 80                
        for j in range(10):
            x1 = 420
            x2 = 450
            for i in range(10):
                self.canvas.create_rectangle(x1, y1, x2, y2, outline = 'white')
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30'''

        self.__init_widgets()
        #start_text = self.canvas.create_text(600, 100, text = "Начало. Правила: \n Размести свои корабли! \n Должно быть:\n 4 однопалубных,\n 3 двухпалубных, \n 2 трёхпалубных, \n 1 четырёхпалубный", font = "TimesNewRoman")

        

    def __init_widgets(self):
        self.canvas.bind("<Button-1>", self.sendMessage)
        self.canvas.bind("<Button-3>", self.canvas_on_click_delete)
        #self.btn_start_game["command"] = self.StartGame
        

    '''def canvas_on_click_create(self,event):
        x = self.winfo_pointerx() - 15 - self.winfo_rootx()
        y = self.winfo_pointery() - 40 - self.winfo_rooty()
        if (45 <= x <= 328) and (45 <= y <= 328):
            ship_id = self.canvas.create_rectangle((x,y,x + 28 ,y + 28), fill = "grey")
            ship = Ship(ship_id,x,y, 10)
            self.ships.append(ship)'''

    def canvas_on_click_delete(self, event):
        shipIds = self.canvas.find_withtag(tk.CURRENT)
        for shipId in shipIds:
            for ship in self.ships:
                if ship.id == shipId:
                    self.canvas.delete(ship.id)
                    self.ships.remove(ship)
    
    '''def StartGame(self):   
        Game = ServerGameWindow(self)
        Game.grap_set()'''

    def sendMessage(self,event):
        msgx = x = self.winfo_pointerx() - 15 - self.winfo_rootx()
        msgy = y = self.winfo_pointery() - 40 - self.winfo_rooty()
        soc.send(str(msgx).encode('utf-8'))
        soc.send(str(msgy).encode('utf-8'))

    def resize(self, event):
        self.w = event.width
        self.h = event.height
    

class ServerGameWindow(tk.Tk):
    def __init__(self, soc):
        super().__init__()
        self.socket = soc
        self.title("Морской бой")
        self.geometry("900x600")
        self.w = 900
        self.h = 600
        self.bind('<Configure>', self.resize)
        self.ships = []
        self.canvas = tk.Canvas(self, bg = "blue")
        self.btn_start_game = tk.Button(self, text = "Старт!")
        self.btn_start_game.pack()
        self.canvas.pack(fill = tk.BOTH, expand = True)
        y = 35
        x = 65
        letters = "АБВГДЕЖЗИК"
        for i in range(10):
            self.canvas.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
            i += 1 
            x += 30

        y = 65
        x = 35
        numbers = 1               
        for j in range(10):
            self.canvas.create_text(x, y, text=str(numbers), font="TimesNewRoman")
            numbers += 1
            y += 30

        y1 = 50
        y2 = 80                
        for j in range(10):
            x1 = 50
            x2 = 80
            for i in range(10):
                self.canvas.create_rectangle(x1, y1, x2, y2, outline = 'white')
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30

        '''y = 35
        x = 435
        letters = "АБВГДЕЖЗИК"
        for i in range(10):
            self.canvas.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
            i += 1 
            x += 30

        y = 65
        x = 405
        numbers = 1               
        for j in range(10):
            self.canvas.create_text(x, y, text=str(numbers), font="TimesNewRoman")
            numbers += 1
            y += 30

        y1 = 50
        y2 = 80                
        for j in range(10):
            x1 = 420
            x2 = 450
            for i in range(10):
                self.canvas.create_rectangle(x1, y1, x2, y2, outline = 'white')
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30'''

        self.__init_widgets()
        start_text = self.canvas.create_text(600, 100, text = "Начало. Правила: \n Размести свои корабли! \n Должно быть:\n 4 однопалубных,\n 3 двухпалубных, \n 2 трёхпалубных, \n 1 четырёхпалубный", font = "TimesNewRoman")

        

    def __init_widgets(self):
        self.canvas.bind("<Button-1>", self.canvas_on_click_create)
        self.canvas.bind("<Button-3>", self.canvas_on_click_delete)
        self.btn_start_game["command"] = self.StartGame
        

    def canvas_on_click_create(self,event):
        y1 = 50
        y2 = 80
        rectangle_count = []            
        for j in range(10):
            x1 = 50
            x2 = 80
            for i in range(10):
                xy_cur = []
                xy_cur.append(x1)
                xy_cur.append(x2)
                xy_cur.append(y1)
                xy_cur.append(y2)
                rectangle_count.append(xy_cur)
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30
        x = self.winfo_pointerx() -  self.winfo_rootx()
        y = self.winfo_pointery() - 25 - self.winfo_rooty()
        #if (45 <= x <= 328) and (45 <= y <=328):
        for i in rectangle_count:
            for j in range(1):
                x01 = i[0]
                x02 = i[1]
                y01 = i[2]
                y02 = i[3]
                if x > x01 and x < x02 and y > y01 and y < y02:
                    ship_id = self.canvas.create_rectangle((x01, y01, x02, y02), fill = "gray")
                    ship = Ship(ship_id, x01, y01, x02, y02, 10)
                    self.ships.append(ship)
        
    def canvas_on_click_delete(self, event):
        shipIds = self.canvas.find_withtag(tk.CURRENT)
        for shipId in shipIds:
            for ship in self.ships:
                if ship.id == shipId:
                    self.canvas.delete(ship.id)
                    self.ships.remove(ship)
    
    def canvas_on_click_destroy(self,event, messagex, messagey):
        x = int(messagex)
        y = int(messagey)
        if (45 <= x <= 328) and (45 <= y <= 328):
            shipIds = self.canvas.find_withtag(tk.CURRENT)
            for shipId in shipIds:
                for ship in self.ships:
                    if ship.id == shipId:
                        self.canvas.delete(ship.id)
                        self.ships.remove(ship)

    def StartGame(self):
        Game = ClientGameWindow(self, self.socket)
        Game.grap_set()


    def resize(self, event):
        self.w = event.width
        self.h = event.height


'''class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Морской бой")
        self.geometry("640x480")
        
        self.__init_widgets()


    def __init_widgets(self):
        labelmenu = tk.Label(self, text = "Меню")
        labelmenu.pack()
        btnStartGame = tk.Button(self, text = "Начать игру")
        btnStartGame.pack()
        btnStartGame["command"] = self.btn_on_click
        btnExit = tk.Button(self, text = "Выход", command=self.destroy)
        btnExit.pack()

    def btn_on_click(self):
        if mb.askyesno('Морской бой',"Вы точно хотите начать игру?!"):
            win = startGameWindow(self)
            win.grap_set()'''

     

if mb.askyesno('Морской бой',"Вы сервер?"):
    #server--------------------------------------------------        
    HOST = "0.0.0.0"
    PORT = 12345


    def client_receive(client_soc:s.socket):
        life = True
        while life:
            try:
                msgx = client_soc.recv(2)
                messagex = msgx.decode('utf-8')
                print(messagex)
                msgy = client_soc.recv(4096)
                messagey = msgy.decode('utf-8')
                print(messagey)
                app.canvas_on_click_destroy(messagex, messagey)
            except:
                life = False
        #clients.remove(client_soc)


    UserNameServer = input("Введите ваше имя пользователя: ")
    soc = s.socket()
    soc.bind((HOST, PORT))
    soc.listen(1)

    
    client_soc, client_addr = soc.accept()
    print('Новое подключение с адреса {}:{}'.format(*client_addr))
    client_t = t.Thread(target=client_receive, args=(client_soc,))
    client_t.start()

    if __name__ == "__main__":
        server = ServerGameWindow(soc)
        server.mainloop()
        

else:
    #client---------------------------------------------------
    HOST = "localhost"
    PORT = 12345
    def client_receive(soc: s.socket):
        life = True
        while life:
            try:
                msgx = soc.recv(4096)
                messagex = msgx.decode('utf-8')
                msgy = soc.recv(4096)
                messagey = msgy.decode('utf-8')
                app.canvas_on_click_destroy(messagex, messagey) 
            except Exception as ex:
                life = False
                print("close", ex)


    soc = s.socket()
    try:
        soc.connect((HOST, PORT))
    except Exception as ex:
        print("Ошибка подключения: ", ex)
        exit(1)
    
    receive_thread = t.Thread(target=client_receive, args=(soc,))
    receive_thread.start()
    UserNameClient = input("Введите ваше имя пользователя: ")

    if __name__ == "__main__":
        server = ServerGameWindow(soc)
        server.mainloop()
    #soc.close()
    #receive_thread.join()   


'''if __name__ == "__main__":
    app = ServerGameWindow()
    app.mainloop()

soc.close()
receive_thread.join()'''