import tkinter as tk
#import tkinter.messagebox as mb
#import tkinter.filedialog as fd

class Ship():
    def __init__(self, id, x01, x02, y01, y02, r):
        super().__init__()
        self.x01 = x01
        self.x02 = x02
        self.y01 = y01
        self.y02 = y02
        self.r = r
        self.id = id



class Frame1(tk.Frame):
    def __init__(self,app):
        super().__init__(app)
        #self.app = app
        self.grid(row = 0, column = 0, sticky ="nsew")
        self.frame1 = tk.Frame(self)
        self.can1 = tk.Canvas(self, bg = "blue")
        self.btn_start_game = tk.Button(self, text = "Старт!")
        self.btn_start_game.pack()
        self.can1.pack(fill = tk.BOTH, expand = True)
        self.ships = []
        y = 35
        x = 65
        letters = "АБВГДЕЖЗИК"
        for i in range(10):
            self.can1.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
            i += 1 
            x += 30
        y = 65
        x = 35
        numbers = 1               
        for j in range(10):
            self.can1.create_text(x, y, text=str(numbers), font="TimesNewRoman")
            numbers += 1
            y += 30

            y1 = 50
            y2 = 80               
            for j in range(10):
                x1 = 50
                x2 = 80
                for i in range(10):
                    self.can1.create_rectangle(x1, y1, x2, y2, outline = 'white')
                    x1 += 30
                    x2 += 30
                y1 += 30
                y2 += 30

        self.__init_widgets()
        start_text = self.can1.create_text(200, 450, text = "Начало. Правила: \n Размести свои корабли! \n Должно быть:\n 4 однопалубных,\n 3 двухпалубных, \n 2 трёхпалубных, \n 1 четырёхпалубный", font = "TimesNewRoman", fill = "white")
    

    def __init_widgets(self):
        self.can1.bind("<Button-1>", self.canvas_on_click_create)
        self.can1.bind("<Button-3>", self.canvas_on_click_delete)
        self.btn_start_game["command"] = self.StartGame
        

    def canvas_on_click_create(self,event):
        y1 = 50
        y2 = 80
        rectangle_count = []            
        for j in range(10):
            x1 = 50
            x2 = 80
            for i in range(10):
                xy_cur = []
                xy_cur.append(x1)
                xy_cur.append(x2)
                xy_cur.append(y1)
                xy_cur.append(y2)
                rectangle_count.append(xy_cur)
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30
        x = self.winfo_pointerx() -  self.winfo_rootx()
        y = self.winfo_pointery() - 25 - self.winfo_rooty()
        #if (45 <= x <= 328) and (45 <= y <=328):
        for i in rectangle_count:
            for j in range(1):
                x01 = i[0]
                x02 = i[1]
                y01 = i[2]
                y02 = i[3]
                if x > x01 and x < x02 and y > y01 and y < y02:
                    ship_id = self.can1.create_rectangle((x01, y01, x02, y02), fill = "gray")
                    ship = Ship(ship_id, x01, y01, x02, y02, 10)
                    self.ships.append(ship)
        
    def canvas_on_click_delete(self, event):
        shipIds = self.can1.find_withtag(tk.CURRENT)
        for shipId in shipIds:
            for ship in self.ships:
                if ship.id == shipId:
                    self.can1.delete(ship.id)
                    self.ships.remove(ship)
   
    def StartGame(self):
        for _ in range(len(self.ships)):
            for ship in self.ships:
                #self.can1.delete(ship.id)
                self.ships.remove(ship)

        #frame2 = Frame2(self)

        

class Frame2(tk.Frame):
    def __init__(self,app):
        super().__init__(app)
        #self.app = app
        self.grid(row = 0, column = 1, sticky ="nsew")
        self.frame2 = tk.Frame(self)
        self.can2 = tk.Canvas(self, bg = "blue")
        self.btn_start_game = tk.Button(self, text = "Старт!")
        self.btn_start_game.pack()
        self.can2.pack(fill = tk.BOTH, expand = True)
        self.ships = []
        y = 35
        x = 65
        letters = "АБВГДЕЖЗИК"
        for i in range(10):
            self.can2.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
            i += 1 
            x += 30
        y = 65
        x = 35
        numbers = 1               
        for j in range(10):
            self.can2.create_text(x, y, text=str(numbers), font="TimesNewRoman")
            numbers += 1
            y += 30

            y1 = 50
            y2 = 80               
            for j in range(10):
                x1 = 50
                x2 = 80
                for i in range(10):
                    self.can2.create_rectangle(x1, y1, x2, y2, outline = 'white')
                    x1 += 30
                    x2 += 30
                y1 += 30
                y2 += 30

        self.__init_widgets()

    def __init_widgets(self):
        self.can2.bind("<Button-1>", self.canvas_on_click_create)
        self.can2.bind("<Button-3>", self.canvas_on_click_delete)
        self.btn_start_game["command"] = self.StartGame
        

    def canvas_on_click_create(self,event):
        y1 = 50
        y2 = 80
        rectangle_count = []            
        for j in range(10):
            x1 = 50
            x2 = 80
            for i in range(10):
                xy_cur = []
                xy_cur.append(x1)
                xy_cur.append(x2)
                xy_cur.append(y1)
                xy_cur.append(y2)
                rectangle_count.append(xy_cur)
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30
        x = self.winfo_pointerx() -  self.winfo_rootx()
        y = self.winfo_pointery() - 25 - self.winfo_rooty()
        #if (45 <= x <= 328) and (45 <= y <=328):
        for i in rectangle_count:
            for j in range(1):
                x01 = i[0]
                x02 = i[1]
                y01 = i[2]
                y02 = i[3]
                if x > x01 and x < x02 and y > y01 and y < y02:
                    ship_id = self.can2.create_rectangle((x01, y01, x02, y02), fill = "gray")
                    ship = Ship(ship_id, x01, y01, x02, y02, 10)
                    self.ships.append(ship)
        
    def canvas_on_click_delete(self, event):
        shipIds = self.can2.find_withtag(tk.CURRENT)
        for shipId in shipIds:
            for ship in self.ships:
                if ship.id == shipId:
                    self.can2.delete(ship.id)
                    self.ships.remove(ship)
        
    def StartGame(self):
        for _ in range(len(self.ships)):
            for ship in self.ships:
                #self.can1.delete(ship.id)
                self.ships.remove(ship)
                
class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Мороской бой")
        self.geometry("900x600")
        self.w = 900
        self.h = 600
        self.bind('<Configure>', self.resize)
        frame1 = Frame1(self)
        frame2 = Frame2(self)
       
    def resize(self, event):
        self.w = event.width
        self.h = event.height








app = App()
#frame1 = Frame1(app)
app.rowconfigure(0, weight =1)
app.columnconfigure(0, weight =1)
app.mainloop()




















'''
def create_frame():
    frame1 = tk.LabelFrame()
    frame1.pack(side = tk.LEFT)
    can1 = tk.Canvas(bg = "blue")
    can1.pack(fill = tk.BOTH, expand = True)
    y = 35
    x = 65
    letters = "АБВГДЕЖЗИК"
    for i in range(10):
        can1.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
        i += 1 
        x += 30
    y = 65
    x = 35
    numbers = 1               
    for j in range(10):
        can1.create_text(x, y, text=str(numbers), font="TimesNewRoman")
        numbers += 1
        y += 30

        y1 = 50
        y2 = 80               
        for j in range(10):
            x1 = 50
            x2 = 80
            for i in range(10):
                can1.create_rectangle(x1, y1, x2, y2, outline = 'white')
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30
    
    frame2 = tk.LabelFrame()
    frame2.pack(side = tk.RIGHT)
    can2 = tk.Canvas(bg = "blue")
    can2.pack(fill = tk.BOTH, expand = True)
    y = 35
    x = 65
    letters = "АБВГДЕЖЗИК"
    for i in range(10):
        can2.create_text(x, y, text=str(letters[i]), font="TimesNewRoman")
        i += 1 
        x += 30
    y = 65
    x = 35
    numbers = 1               
    for j in range(10):
        can2.create_text(x, y, text=str(numbers), font="TimesNewRoman")
        numbers += 1
        y += 30

        y1 = 50
        y2 = 80               
        for j in range(10):
            x1 = 50
            x2 = 80
            for i in range(10):
                can2.create_rectangle(x1, y1, x2, y2, outline = 'white')
                x1 += 30
                x2 += 30
            y1 += 30
            y2 += 30

root = tk.Tk()
root.geometry('1024x900')
btn = tk.Button(text = "create new Frame")
btn.pack()
btn ["command"] = create_frame

#oval = can1.create_oval((20,20,40,40), fill = "red")

root.mainloop()
'''

'''class Window(tk.Tk):
    def __init__(self):
        super().__init__()

class tpl(tk.Toplevel):
    def __init__(self,master):
        super().__init__(master)

win = tk.Tk()
win.configure(bg = "black")
class frame1(tk.Frame):
    def __init__(self):
        super().__init__()
        self.c1 = tk.Canvas(self, bg = "red")
        self.c1.grid(row = 0, column = 0)
        line = self.c1.create_oval((5,5,10,10), fill = "red")

class frame2(tk.Frame):
    def __init__(self):
        super().__init__()
        self.F2 = tk.Frame(win, width = 250, heigh = 250, bg = "white")
        self.F2.grid(row = 0, column = 1,padx = 1)
        self.c2 = tk.Canvas(self, bg = "blue")
        

f1 = frame1()
f2 = frame2()
win.mainloop()'''