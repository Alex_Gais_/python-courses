def table(n: int,m: int,d: int,val: int):
    for k in range(0,n):
        if k == 0:
            print("\u2554", end ="")
            for j in range(0,m):
                for i in range(d):
                    print("\u2550", end ="")
                if j == (m-1):
                    print("\u2557")
                else:
                    print("\u2566", end="")     
            for i in range(m+1):
                if i == m:
                    print("\u2551")
                elif i < m:
                    print(("\u2551{:^" + str(d)+"}").format(str(val)), end="")
                    val += 1
            if k < n-1:
                print("\u2560", end="")
                for j in range(0,m):
                    for i in range(d):
                        print("\u2550", end ="")
                    if j == (m-1):
                        print("\u2563")
                    else:
                        print("\u256C", end="")
            if k == n-1:
                print("\u255A", end="")
                for j in range(0,m):
                    for i in range(d):
                        print("\u2550", end ="")
                    if j == (m-1):
                        print("\u255D")
                    else:
                        print("\u2569", end="")
        elif k > 0:
            for i in range(m+1):
                if i == m:
                    print("\u2551")
                elif i < m:
                    print(("\u2551{:^" + str(d)+"}").format(str(val)), end="")
                    val += 1
            if k < n-1:
                print("\u2560", end="")
                for j in range(0,m):
                    for i in range(d):
                        print("\u2550", end ="")
                    if j == (m-1):
                        print("\u2563")
                    else:
                        print("\u256C", end="")
            if k == n-1:
                print("\u255A", end="")
                for j in range(0,m):
                    for i in range(d):
                        print("\u2550", end ="")
                    if j == (m-1):
                        print("\u255D")
                    else:
                        print("\u2569", end="")