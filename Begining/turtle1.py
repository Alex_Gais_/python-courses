'''import turtle # Подключаем модуль turtle
turtle.Screen().setup(800, 600) # Задать размеры окна
turtle.shape("turtle") # Использовать каринку черепашки
turtle.reset() # Приводим черепашку в начальное положение
turtle.down() # Опускаем перо перо (начало рисования)
turtle.forward(200) # Проползти 200 пикселей вперед
turtle.left(90) # Поворот влево на 90 градусов
turtle.forward(200) # Рисуем вторую сторону квадрата
turtle.left(90)
turtle.forward(200) # Рисуем третью сторону квадрата
turtle.left(90)
turtle.forward(200) # Рисуем четвертую сторону квадрата
turtle.penup() # Поднять перо (закончить рисовать)
turtle.forward(100) # Отвести черепашку от рисунка в сторону
turtle.Screen().mainloop() # Задержать окно на экране
'''
'''from turtle import *
from math import sin, cos, pi

FORCE_UNIT = 0.1 # Ускорение двигателя
HEIGHT = 400
WIDTH = 400

def one_step(): # Одна итерация движения
    global speed_x, speed_y # Глобальные переменные, в которых хранится текущая скорость
    cur_x, cur_y = position() # Получаем текущие координат
    cur_x += speed_x # Двигаем "корабль"
    cur_y += speed_y
    setpos(cur_x, cur_y)
    if abs(cur_x) > WIDTH: # Проверяем, что не улетели слишком далеко
        speed_x *= -1
    if abs(cur_y) > HEIGHT:
        speed_y *= -1

def turn_left(): # Была нажата клавиша "Налево"
    left(10)

def turn_right(): # Была нажата клавиша "Направо"
    right(10)
def turtle_pendown():
    pendown()
def turtle_penup():
    penup()
def exit(): # Была нажата клавиша Escape, завершаем работу
    global continue_game
    continue_game = False

def speed_up(): # Была нажата клавиша "Вверх"
    global speed_x, speed_y # Глобальные переменные, в которых хранится текущая скорость
    alpha = heading() * pi / 180.0 # Получаем текущий угол в радианах
    speed_x += FORCE_UNIT * cos(alpha) # Ускоряемся
    speed_y += FORCE_UNIT * sin(alpha)

def speed_down(): # Была нажата клавиша "Вверх"
    global speed_x, speed_y # Глобальные переменные, в которых хранится текущая скорость
    alpha = heading() * pi / 180.0 # Получаем текущий угол в радианах
    speed_x -= FORCE_UNIT * cos(alpha) # Ускоряемся
    speed_y -= FORCE_UNIT * sin(alpha)

def main():
    reset()
    global speed_x, speed_y # Глобальные переменные, в которых хранится текущая скорость
    global continue_game
    speed_x = speed_y = 0
    continue_game = True
    speed(0) # Перемещаться будем мгновенно
    pensize(2) # И рисовать за собой линию
    shape("turtle") # Форма курсора
    fillcolor("blue") # Цвет черепашки
    pencolor("red") # Цвет линии

    onkeypress(turtle_pendown,"3")
    onkeypress(turtle_penup,"9")
    onkeypress(turn_left, "Left") # По нажатию кнопки "Налево" вызвать функцию turnleft()
    onkeypress(turn_right, "Right") # ...
    onkeypress(speed_up, "Up") # ...
    onkeypress(speed_down, "Down")
    onkey(exit, "Escape") # По нажатию кнопки Escape завершить работу
    listen() # Начинаем "слушать" нажатия клавиш
    while continue_game:
        one_step() # Запускаем основной цикл
    bye()


main()'''

#turtle.
import turtle
turtle.pencolor("blue")
turtle.penup()
turtle.listen()
height = 300
width = 350
x = turtle.xcor()
y = turtle.ycor()

def turtle_pendown():
    turtle.pendown()
    return
def turtle_penup():
    turtle.penup()
    return
def drive_forward():
    x , y = turtle.position()
    if abs(x) > width:
        turtle.right(180)
        turtle.forward(10)
    else:
        turtle.forward(10)
    if abs(y) > height:
        turtle.right(180)
        turtle.forward(10)
    else:
        turtle.forward(5)
    return
def drive_backward():
    x , y = turtle.position()
    if abs(x) > width:
        turtle.right(180)
        turtle.forward(10)
    else:
        turtle.backward(10)
    if abs(y) > height:
        turtle.right(180)
        turtle.forward(10)
    else:
        turtle.backward(5)
    return
def drive_left():
    turtle.left(10)
    return
def drive_right():
    turtle.right(10)
    return
def turtle_home():
    turtle.home()
    return

turtle.listen()
turtle.onkeypress(drive_forward,"w")
turtle.onkeypress(drive_backward,"s")
turtle.onkeypress(drive_left,"a")
turtle.onkeypress(drive_right,"d")
turtle.onkeypress(turtle_pendown,"q")
turtle.onkeypress(turtle_penup,"e")
turtle.onkeypress(turtle_home,"r")
input()